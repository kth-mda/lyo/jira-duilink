# Jira DUI Link plugin

> This plugin allows adding links to Jira issues that point to OSLC Resources.

## Getting started

Jira development commands (after installing the [SDK][1]):

* `atlas-run`   -- installs this plugin into the product and starts it on `localhost`
* `atlas-package` -- creates JAR and OBI packages that can be uploaded via Web UI. Also triggers auto-reload of the debug instance.

Other commands:

* `atlas-debug` -- same as `atlas-run`, but allows a debugger to attach at port `5005`
* `atlas-cli`   -- after atlas-run or atlas-debug, opens a Maven command line window:
   - `pi` reinstalls the plugin into the running product instance
* `atlas-help`  -- prints description for all commands in the SDK

> **NB!** Be sure to set `JAVA_HOME` properly (e.g. `export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_77.jdk/Contents/Home` on macOS). 

[1]: https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK


## License

> Copyright 2023 KTH Royal Institute of Technology
> 
> Licensed under the EUPL-1.2-or-later, with extension of article 5 (compatibility clause) to any licence for distributing derivative works that have been produced by the normal use of the Work as a library.