package se.kth.md.assume.jira;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.AbstractCustomFieldType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.base.Strings;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.NotNullPredicate;
import org.apache.log4j.Logger;

/**
 * All the other Multi* classes refer to Users or Options. This class,
 * like VersionCFType, uses a different transport object, a Collection
 * of Carrier objects.
 * <p>
 * The changes for JIRA 5.0 mean that the transport and singular types
 * have to be given as a parameter to AbstractCustomFieldType. Also
 * <p>
 * More information can be found at
 * "https://developer.atlassian.com/display/JIRADEV/Java+API+Changes+in+JIRA+5.0
 * #JavaAPIChangesinJIRA50-CustomFieldTypes
 */
@Scanned
public class OslcLinksCFType extends AbstractCustomFieldType<Collection<OslcLink>, OslcLink> {

  private static final Logger log = Logger.getLogger(OslcLinksCFType.class);
  /**
   * Used in the database representation of a singular value.
   * Treated as a regex when checking text input.
   */
  private static final String DB_SEP = "###";
  // The type of data in the database, one entry per value in this field
  private static final PersistenceFieldType DB_TYPE = PersistenceFieldType.TYPE_UNLIMITED_TEXT;
  private final CustomFieldValuePersister persister;
  private final GenericConfigManager genericConfigManager;

  public OslcLinksCFType(
    @ComponentImport final CustomFieldValuePersister customFieldValuePersister,
    @ComponentImport final GenericConfigManager genericConfigManager) {
    this.persister = customFieldValuePersister;
    this.genericConfigManager = genericConfigManager;
  }

  @Override
  public String getStringFromSingularObject(OslcLink singularObject) {
    // TODO: 12.02.17 is it the right call? 
    return singularObject.toString();
  }

  @Override
  public OslcLink getSingularObjectFromString(String dbValue) throws FieldValidationException {
    log.debug("getSingularObjectFromString: " + dbValue);
    if (Strings.isNullOrEmpty(dbValue)) {
      return null;
    }
    String[] parts = dbValue.split(DB_SEP);
    if (parts.length == 0 || parts.length > 2) {
      log.warn("Invalid database value for MultipleValuesCFType ignored: " + dbValue);
      // If this should not be allowed, then throw a
      // FieldValidationException instead
      return null;
    }
    String url = parts[0];
    String title = "";
    if (parts.length == 2) {
      title = parts[1];
    }
    return new OslcLink(url, title);
  }

  /**
   * For removing the field, not for removing one value
   */
  @Override
  public Set<Long> remove(CustomField field) {
    return persister.removeAllValues(field.getId());
  }

  /**
   * Validate the input from the web pages, a Collection of Strings.
   * Exceptions raised later on after this has passed appear as an
   * ugly page.
   */
  @Override
  public void validateFromParams(CustomFieldParams relevantParams,
    ErrorCollection errorCollectionToAddTo, FieldConfig config) {
    log.debug("validateFromParams: " + relevantParams.getKeysAndValues());
    try {
      getValueFromCustomFieldParams(relevantParams);
    } catch (FieldValidationException fve) {
      errorCollectionToAddTo.addError(config.getCustomField().getId(), fve.getMessage());
    }
  }

  @Override
  public void createValue(CustomField field, Issue issue, Collection<OslcLink> value) {
    if (value != null) {
      persister.createValues(field, issue.getId(), DB_TYPE, getDbValueFromCollection(value));
    } else {
      log.error("value shall be a non-null collection");
    }
  }

  @Override
  public void updateValue(CustomField field, Issue issue, Collection<OslcLink> value) {
    persister.updateValues(field, issue.getId(), DB_TYPE, getDbValueFromCollection(value));
  }

  /**
   * Extract a transport object from the string parameters,
   * Clearing an amount removes the row.
   */
  @Override
  public Collection<OslcLink> getValueFromCustomFieldParams(CustomFieldParams parameters)
    throws FieldValidationException {
    log.debug("getValueFromCustomFieldParams: " + parameters.getKeysAndValues());
    // Strings in the order they appeared in the web page
    final Collection values = parameters.getAllValues();
    if ((values == null) || values.isEmpty()) {
      return null;
    }
    Collection<OslcLink> value = new ArrayList<>();
//    Object[] valuesArray = values.toArray();
//    if ((valuesArray.length % 2) != 0) {
//      log.error("values collection must have an even number of elements");
//    }
//    for (int i = 0; i < valuesArray.length - 1; i += 2) {
//      String url = (String) valuesArray[i];
//      String title = (String) valuesArray[i + 1];
//      if (Strings.isNullOrEmpty(url)) {
//        throw new FieldValidationException("URL must not be empty");
//      }
//      if (title == null) {
//        title = "";
//      }
//      if (title.contains(DB_SEP)) {
//        throw new FieldValidationException(
//          "Title must not contain reserved sequence of characters '" + DB_SEP + "'");
//      }
//
//      value.add(new OslcLink(url, title));
//    }

    for (Object v : values) {
      String inputString = (String) v;
      log.warn("Input string: '"+inputString+"'");
      String[] inputElements = inputString.split("\u2626");
      if(inputElements.length == 2) {
        value.add(new OslcLink(inputElements[0], inputElements[1]));
      } else {
        log.error(String.join(" § ", inputElements));
      }
    }

    return value;
  }

  /**
   * This method is used to create the $value object in Velocity templates.
   */
  @Override
  public Object getStringValueFromCustomFieldParams(CustomFieldParams parameters) {
    log.debug("getStringValueFromCustomFieldParams: " + parameters.getKeysAndValues());
    return parameters.getAllValues();
  }

  @Override
  public Collection<OslcLink> getValueFromIssue(CustomField field, Issue issue) {
    // This is also called to display a default value in view.vm
    // in which case the issue is a dummy one with no key
    if (issue == null || issue.getKey() == null) {
      log.debug("getValueFromIssue was called with a dummy issue for default");
      return null;
    }

    // These are the database representation of the singular objects
    final List<Object> values = persister.getValues(field, issue.getId(), DB_TYPE);
    log.debug("getValueFromIssue entered with " + values);
    if ((values != null) && !values.isEmpty()) {
      List<OslcLink> result = new ArrayList<>();
      for (final Object value : values) {
        String dbValue = (String) value;
        OslcLink oslcLink = getSingularObjectFromString(dbValue);
        if (oslcLink == null) {
          continue;
        }
        result.add(oslcLink);
      }
      return result;
    } else {
      return null;
    }
  }

  /**
   * Retrieve the stored default value (if any) from the database
   * and convert it to a transport object (a Collection of Carrier
   * objects).
   */
  @Override
  public Collection<OslcLink> getDefaultValue(FieldConfig fieldConfig) /* */{
    final Object o = genericConfigManager.retrieve(CustomFieldType.DEFAULT_VALUE_TYPE,
      fieldConfig.getId().toString());
    log.debug("getDefaultValue with database value " + o);

    Collection<OslcLink> collectionOfOslcLinks = null;
    if (o instanceof Collection) {
      collectionOfOslcLinks = (Collection) o;
    } else if (o instanceof OslcLink) {
      log.warn("Backwards compatible default value, should not occur");
      collectionOfOslcLinks = EasyList.build((OslcLink) o);
    }

    if (collectionOfOslcLinks == null) {
      return null; // No default value exists
    }

    // Convert a database value (String) to a singular Object (Carrier)
    final Collection collection = CollectionUtils.collect(collectionOfOslcLinks, input -> {
      if (input == null) {
        return null;
      }
      String dbValue = (String) input;
      return getSingularObjectFromString(dbValue);
    });
    CollectionUtils.filter(collection, NotNullPredicate.getInstance());
    log.debug("getDefaultValue returning " + collection);
    return collection;
  }

  /**
   * Convert a transport object (a Collection of Carrier objects) to
   * its database representation and store it in the database.
   */
  @Override
  public void setDefaultValue(FieldConfig fieldConfig, Collection<OslcLink> value) {
    log.debug("setDefaultValue with object " + value);
    Collection<?> carrierStrings = getDbValueFromCollection(value);
    if (carrierStrings != null) {
      carrierStrings = new ArrayList<>(carrierStrings);
      genericConfigManager.update(CustomFieldType.DEFAULT_VALUE_TYPE,
        fieldConfig.getId().toString(), carrierStrings);
    }
  }

  @Override
  public String getChangelogValue(CustomField field, Collection<OslcLink> value) {
    if (value == null) {
      return "";
    }
    //    StringBuilder sb = new StringBuilder();
//    for (OslcLink oslcLink : value) {
//      sb.append(oslcLink.toString());
//      // Newlines are text not HTML here
//      sb.append(", ");
//
//    }
//    return sb.toString();
    return String.join(", ",
      value.stream().map(OslcLink::toString).collect(Collectors.toList()));
  }

  // Helper Methods

  /**
   * Convert the Transport object to a collection of the
   * representation used in the database.
   */
  private Collection getDbValueFromCollection(final Collection<OslcLink> linkCollection) {
    log.debug("getDbValueFromCollection: " + linkCollection);
    if (linkCollection == null) {
      return Collections.EMPTY_LIST;
    }
    List<String> dbValue = new ArrayList<>();
    for (OslcLink oslcLink : linkCollection) {
      if (oslcLink == null) {
        continue;
      }
      String value = valueElementToString(oslcLink);
      dbValue.add(value);
    }
    return dbValue;
  }

  private String valueElementToString(final OslcLink oslcLink) {
    return oslcLink.getUrl() + DB_SEP + oslcLink.getTitle();
  }

  /**
   * Copied from the auto-generated code
   */
  @Override
  @Deprecated
  public Map<String, Object> getVelocityParameters(final Issue issue,
    final CustomField field,
    final FieldLayoutItem fieldLayoutItem) {
    final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

    // This method is also called to get the default value, in
    // which case issue is null so we can't use it to add currencyLocale
    if (issue == null) {
      return map;
    }

    FieldConfig fieldConfig = field.getRelevantConfig(issue);
    //add what you need to the map here

    return map;
  }

}
