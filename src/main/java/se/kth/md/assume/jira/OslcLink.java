package se.kth.md.assume.jira;

public class OslcLink {
    
    private String url;
    private String title;
    
    public OslcLink(String url, String title) {
        this.url = url;
        this.title = title;
    }
    
    public String getUrl() {
        return url;
    }
    
    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OslcLink{");
        sb.append("url='").append(url).append('\'');
        sb.append(", title='").append(title).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
