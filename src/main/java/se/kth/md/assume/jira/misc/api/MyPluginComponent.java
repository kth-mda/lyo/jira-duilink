package se.kth.md.assume.jira.misc.api;

public interface MyPluginComponent {
  String getName();
}
