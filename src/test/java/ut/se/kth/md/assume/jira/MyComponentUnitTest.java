package ut.se.kth.md.assume.jira;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import se.kth.md.assume.jira.misc.api.MyPluginComponent;
import se.kth.md.assume.jira.misc.api.MyPluginComponentImpl;

public class MyComponentUnitTest {
  @Test
  public void testMyName() {
    MyPluginComponent component = new MyPluginComponentImpl(null);
    assertEquals("names do not match!", "myComponent", component.getName());
  }
}
